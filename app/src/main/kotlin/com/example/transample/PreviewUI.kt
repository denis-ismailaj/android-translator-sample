package com.example.transample

import android.Manifest
import androidx.camera.core.Preview
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.viewinterop.AndroidView
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun PreviewUI(preview: Preview) {
    // Store camera runtime permission state
    val cameraPermissionState = rememberPermissionState(
        Manifest.permission.CAMERA
    )

    // Request permission once automatically on launch
    LaunchedEffect(Unit) {
        cameraPermissionState.launchPermissionRequest()
    }

    when (cameraPermissionState.status) {
        is PermissionStatus.Denied -> {
            // In case user denies the first time, leave the possibility to try again.
            Column {
                Button(onClick = { cameraPermissionState.launchPermissionRequest() }) {
                    Text("Request camera permission")
                }
            }
        }
        else -> {
            // Set up camera preview UI
            AndroidView(factory = { context ->
                PreviewView(context).also {
                    preview.setSurfaceProvider(it.surfaceProvider)
                }
            })
        }
    }
}
