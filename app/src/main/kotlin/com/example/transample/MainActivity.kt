package com.example.transample

import android.os.Bundle
import android.util.Size
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.foundation.layout.Column
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.guava.await
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

class MainActivity : ComponentActivity(), TranslationAnalyzer.Listener {

    // Camera provider will be retrieved by the system later
    private lateinit var cameraProvider: ProcessCameraProvider

    // Initialize camera preview lazily
    private val preview by lazy { Preview.Builder().build() }

    // Set up camera configuration to use rear camera
    private val cameraSelector: CameraSelector by lazy {
        CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()
    }

    // Flows used to update UI when translations are made
    private val detectedLanguage = MutableStateFlow("")
    private val translatedText = MutableStateFlow("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainUI()
        }

        // Do async work using a lifecycle-aware coroutine
        lifecycleScope.launch {
            // Get a provider for the camera from the system.
            cameraProvider = ProcessCameraProvider.getInstance(this@MainActivity).await()

            // Configure the analysis pipeline
            val imageAnalysis = ImageAnalysis.Builder()
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST) // drop intermediate images
                .setTargetResolution(Size(1280, 720)) // reduced resolution to improve performance
                .build()
                .also {
                    // Set up our custom analyser
                    it.setAnalyzer(
                        Executors.newSingleThreadExecutor(),
                        TranslationAnalyzer(this@MainActivity)
                    )
                }

            // Tie the analysis process and the preview to the app's lifecycle
            cameraProvider.bindToLifecycle(
                this@MainActivity, cameraSelector, imageAnalysis, preview
            )
        }
    }

    override fun onTranslateDone(language: String, text: String) {
        detectedLanguage.value = language
        translatedText.value = text
    }

    @Composable
    private fun MainUI() {
        Column {
            PreviewUI(preview)

            val language = detectedLanguage.collectAsState()
            val text = translatedText.collectAsState()

            if (language.value.isBlank()) {
                CircularProgressIndicator()
            } else {
                Text("Language: " + language.value)
                Text(text.value)
            }
        }
    }
}
