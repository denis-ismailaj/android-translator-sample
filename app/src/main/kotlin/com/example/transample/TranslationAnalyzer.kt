package com.example.transample

import android.util.Log
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.nl.languageid.LanguageIdentification
import com.google.mlkit.nl.translate.TranslateLanguage
import com.google.mlkit.nl.translate.Translation
import com.google.mlkit.nl.translate.TranslatorOptions
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import java.util.concurrent.ExecutionException

class TranslationAnalyzer(
    private val listener: Listener
) : ImageAnalysis.Analyzer {

    private val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

    @OptIn(ExperimentalGetImage::class)
    override fun analyze(imageProxy: ImageProxy) {
        // Get the image if available or return
        val mediaImage = imageProxy.image ?: return
        val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)

        // Run processing on the same thread as we're only doing one analysis at a time.
        // STRATEGY_KEEP_ONLY_LATEST on the MainActivity drops intermediary
        // images that are retrieved while we're doing work here.
        runBlocking {
            try {
                processImage(image)
            } catch (ex: ExecutionException) {
                // Ignore detection failures. The next frame will be tried again very soon.
            }
        }

        imageProxy.close()
    }

    private suspend fun processImage(image: InputImage) {
        // Get recognized text from the image
        val text = recognizer.process(image).await().text

        Log.d("Recognized: ", text)

        // Find what language the text is in.
        // Drop this frame if it couldn't be detected.
        val languageCode = detectLanguage(text) ?: return

        // Translate the text to english from the detected language.
        val translation = translate(languageCode, text) ?: return

        Log.d("Translated: ", translation)

        // Send results back to caller
        listener.onTranslateDone(languageCode, translation)
    }

    private suspend fun detectLanguage(text: String): String? {
        val languageCode = LanguageIdentification.getClient()
            .identifyLanguage(text).await()

        Log.d("Language code: ", languageCode)

        // Check if it couldn't be detected
        if (languageCode == "und") {
            return null
        }

        // Return code when successful
        return languageCode
    }

    private suspend fun translate(sourceLanguage: String, text: String): String? {
        // Set up translator class
        val options = TranslatorOptions.Builder()
            .setSourceLanguage(sourceLanguage)
            .setTargetLanguage(TranslateLanguage.ENGLISH)
            .build()
        val translator = Translation.getClient(options)

        // Download the models if not already downloaded
        translator.downloadModelIfNeeded().await()

        // Return the translated text
        return translator.translate(text).await()
    }

    interface Listener {
        fun onTranslateDone(language: String, text: String)
    }
}
